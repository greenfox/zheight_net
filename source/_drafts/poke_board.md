---
title: poke_board
tags:
---



# Poke Board

Simone Giertz created a fantastic habit formation tool and started a Kickstarter
for it. It's calender made of buttons and lights.

{% raw %}
<iframe width="560" height="315" src="https://www.youtube.com/embed/-lpvy-xkSNA" 
frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; 
picture-in-picture" allowfullscreen></iframe>
{% endraw %}

The idea is simple. Push a button on every day that you do your new habit. Then
the panel lights up for that day. That's it. Then at the end of every month you
can see all the days you did your thing. However, to manage 365 individual
buttons and just as many lights, you need lots of electronics. A normal keyboard
has 104 buttons, and only the more expensive ones are individually back lights. 

I saw this and was really interested until I saw the price tag. Then I starting
to think "well, how could I build this thing?" I think I can build something
similar, but much simpler...

<!--more-->

## The Idea

Instead of poking a button, what if you could just poke a hole? I started to
design a box that had a back light, a diffuser, and a wooden calender that has
tabs for each date.

After juggling with this idea for a while thinking I would have to learn how to
learn woodworking or CNC, I decided the best way to build this was going to be
to laser cut the entire thing, including the frame.

## First Prototype

Since this is going to be a laser cut project, I originally planned to use
InkScape or raw SVG editing to author the shapes I needed for this project, but
then I discovered SolveSpace. SolveSpace is a 3d modeling tool that allows you
to parametrically define geometry.

After learning the tool and some fiddling, I was able to create this.

{% asset_img calender.png %}

SolveSpace allows me to define these circles once and then duplicate them across
the space then define the size of the frame based on their location. Then I can
export as an SVG and laser cut the shape.

Except these aren't exactly circles. They don't meet at the bottom making them
actually a "C" shape with a 0.5mm gap. With a small push, the hole pokes out.

After I have all these defined, I can export this project as an SVG, load it
into InkScape for some minor edits and cleanup (select all, set to nofill, add
an outline), it's ready for the laser cutter.

It looked nice, but I felt the need to draw up a quick stand for it and laser
cut that too. Basically the same process.

{% asset_img stand.png %}

//todo should I include the output SVGs?

//todo take picture of it in use

This is a very functional prototype. Without the light, it doesn't have the same
psychological effect that Simone's design has, but it's a first step. It's
currently sitting next to my dog's food and my roommate and I are using it keep
track of whether or not she's been fed for that day.

