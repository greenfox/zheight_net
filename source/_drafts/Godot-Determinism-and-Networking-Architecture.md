---
title: 'Godot, Determinism, and Networking Architecture'
tags:
---

# Godot, Determinism, and Networking Architecture

I have started a project in Godot that I plan to multiplayer. However, I also
plan on targeting lots of different platforms and OSs. This has put me in a bit
of a bind. For me to explain how, I have explain determinism and some basics
about network architecture. 

## Determinism

What is Determinism? In computer science, something is said to be deterministic
if, given the same inputs, it always gives you the same result every time.

```js
function add(a,b){
    return a + b
}
```

This function, for example should give you exactly the same result every time
you run it with the same input. `add(5,6)` should always return `11`.

Alternatively, a `random()` function should almost always give you a different
result, so it's "nondeterministic". 

> As a sidebar, this isn't really totally true because most random number
generators can be seeded and will give the same results given the same seed.
When Minecraft asks for a seed, it's feeding that number into it's random number
generator so that you just have to shard a seed to share maps.

Ok, so as long as you're not using random numbers or always seed your random
number generator the same, does that mean my code will always run the same way?
Unfortunately, no. Turns out every CPU, OS, and language might handle certain
things slightly differently. Especially rounding errors. Decided what point in a
number with a lot of decimal places is acceptable to drop isn't exactly the same
on every configuration. Usually the same configuration.

You know what uses a lot of float point number math? Game physics! A
deterministic physics simulation would mean every time you run the simulation,
you get *exactly* the same result. If you shake the dice in *exactly* the same
way, you will always every single time get the same result. The dice will land
in exactly the same spot with exactly the same number pointed up. Not close.
Exactly.

Not all game physics engines are deterministic. If the physics engine is at all
tied to framerate, then your simulation wont run quite the same way every time
since the frame step will not be consistent every time. The interesting thing is
that this is a binary: you either are deterministic or you are not. If you have
a complex scene, you won't get "close" results every time. You'll only ever get
exactly the same result or widely different results every time.

## Why does this matter?

For most games, it doesn't. Making sure everything happens *exactly* the same
way on every platform isn't very important as long as it's "close enough".
Player input wont ever be *exactly* the same between runs. It doesn't matter if
the Xbox, Playstation, or PC version don't do exactly the same thing given the
same input because no player would notice the difference.

Except for multiplayer!

## Multiplayer

For multiplayer games, you have two options that we're going to talk about today
(there are others, but we're not going to talk about them here). 


### Deterministic Lockstep

If you can run the simulation exact same way on every device given the exact
same input, then you can do something called "Deterministic Lockstep". Each
player input is captured with a frame number and pushed over the network. When
each client gets the input state from every other player, the runs one step of
the simulation for that frame. 

This is extraordinarily efficient. The ONLY thing that actually needs to
transverse the network is the player's controls which is an absolutely tiny
amount of data. It doesn't matter how complex your scene is, how many objects
you have to track, or how limited your bandwidth is. 

Games like RTSs typically use Deterministic Lockstep. You can have thousands of
units in  the game at a time and as long as each client and run the simulation
at a reasonable speed, then the network bandwidth doesn't increase significantly
with more objects.

Little Big Planet does this. Even though they have potentially thousands of
physics objects, they only need to share player controls to make sure everyone's
shows exactly the same game world.

With all these advantages, why doesn't everyone do this? Because it's hard to
make sure your simulations run exactly the same on every platform. RTSs can get
away with it because they can make sure everything behaves as expectedly on
every platform. Little Big Planet can get away with this because they know
that every Playstation is *exactly* the same in every way that matters.



what is determinsm
why does it matter
multiplayer and determism
godot and determism
