---
title: Twilight Imperium
date: 2019-02-22 20:00:00
tags: [laser,"board games","games"]
---

Twilight Imperium makes Catan look like Candy Land. 

The base game of Catan has 19 tiles. A game of Twilight imperium has 61 tiles
(and that's not even all the tiles out of the box). There are 17 different
unique factions. Each of 6 players has 10 different **types** of plastic pieces.
Some tokens are considered "unlimited" and if you run out, you're expected to
find something to substitute them. Catan has a single Building Costs card to
remind players the rules; Twilight Imperium has a full size sheet of paper
that's different for every player. It comes with 3 books thinker than the single
book that Catan comes with. It's a big game. And it comes in a big box. A very
poorly organized box.

![It's a big game.](https://lh3.googleusercontent.com/dkCDh0fOtU-NHoWft0wdMsbNmoMVPkn22trSKNQ17PqZU1NekCvbW-2EypBKTdf_hXiIjV_y6L2OubtIrP73zufme0pObOBATHIX4H1HWW7kLs-DVA89OLa4NqiOpKkl1QjpG_kmEUuLRJLzxel7_zocF9AJH42ks6f5iEjorftX26Piwtl4kYkvsapJPEM9ak_E22d08KuyesEx2OSSppB0cJbjF9C94xeUUX7JOQ6heAEzGQVvi4q6GKb0SVN3xAieEmQjwfRH7-T_Wqmvba3xutQ1hC65b0dtrnLq6nejxXN68TcipSgCOkvWg_Q-__z43wuJ6WCGUlsiy8huF6YkTKnc6F6Z7sE8N7SzF7ZOZzSPOpeXoRPVJOmwNMx2RyMMqS5V1F4QAvW7BszWOHLYZpLuUsbBeh2UqJMpNYLw-kkavqsUj5rzTU9WtaSodq7EZ2bpHYwvAeYxepS-6rLvY0XXy1SOr2s1y8o7RbFSBmae-bCbdzSmyll4EDvDaPgTQnq5TGa-EnbRw8T_F8b2hXp5nmm1vP5R1myKpCUvPAwulwlfD68EpIyM_p2hRQOEOzE-xVkHbWmqRpwOJmixiUv_VMR8beNDQrXB-ZLglo4mybsE00CR5NHmkpGlf1orbRa3Xky9rZqKShjxdnMtGYn-LsLZ4ILKcsIeAvaKMdAbauzeSsL2kfJAKhq--rwL02N3e231FCM5ziz92FjPNg=w2535-h1901-no)

<!--more-->

## Box Insert

The Twilight Imperium does not adequately help organize the 17 factions, over 70
tiles, 6 colors worth of player pieces that it comes with. What it does comes
with is a vacuum formed plastic organizer, and entirely too few bags. Catan
comes with a similar things. However, that is not nearly enough for Twilight
Imperium. I want player trays, separate storage for each of the 17 factions, and
a way to organize all the other miscellaneous parts that come with it.

I don't need a fully wood cut copy of the game like I did with Catan. I need a
Box Insert. And since I don't have the skills for to design this, I started
seeking one out.

## Finding Plans

There are a few companies that make after market inserts for board games.
However, at the time working on this project, there was only one for Twilight
Imperium that I liked and they only sold the plans. Midgard Creative Studio sold
a kit for $18. I bought it and got to work.

## Cutting

Other than some minor errors in the source files, cutting went pretty well. With
everything I learned from Catan project, it was pretty easy process. Although,
much longer. DMS charges $1 per 5 minutes of laser time. I paid $20. And that
doesn't include time I sat aat the laser getting everything ready for the next
cut.

I had to make 20 boxes for factions. There are only 17 factions, but to make
sure the box fits nicely without any loose space for things to jostle around,
they are stacked in two rows of 10. This went quite easily. The only real
challenge here would have been rasterized text, but the source files already had
speeds and powers configured for that. If I had created this project, I would
have vectorized the text for a much faster and cleaner cut.

![A single faction box](https://lh3.googleusercontent.com/7MvFXGX13NcMlwsMIZqrmimZy0f9pq_AtPSCwNsBvT0TiQ_5EYdViykENXE_Ihgqv42uB8d0-7sq1uC7mOfnZ0k4a_f8c7_qXj8WhYbwoUx0Pu6thFPgPv7XJOAwOLKe1fY2knbDKExlJSZVu7CM_Pfh7dIDtarRqjwBzNcbRZE5SVzP78NsWjyb7o_NAjT5jn4_6yaiiW4SzUVoh6E8q-vgPJIuL0QLPPRrbkZCsyrJlBuNhg7zGnleDyTVQweYktFNOGafzlXZ8G6CEymJ8hrv8ow90tRHyuyU2RYTi7jRe8uhArizQQGO_E2Cm7dspdzA6ZcAQoD4K8uy1-8457mfemo2J-_4GhqOtH2i4sT6jKfSFJW3YgltT6pbIAh9xr3ey_IWrsQDPg55oKsz-D_6L7_SrQ-th-hPRSgYi_wkZ9KBEJysIPdOSfNKgant2fUkEIIK1iX4e7jYdcYd2LvDHHdiJR2XkUYa8A32uX0CJL6l6CKxZlSRfYa7ExNA7zFMJe9lNvoBY88QHoUHEXhtitiu53FBj0PSlKGKMCF5nLMqRIugC-r-qq2gRal_YKjBaEWh4pWDIM9YbK3Q5T1ka7R_XOIpo4x9tTrydRoRSJ84qDbeuvNZ7TmWUEmWJGz0F71Ggn4LegSGSffhNHvsZxeaWjO52O1OOXMfCYYhngNImrTgAKGYeG73k319YJoN9tq6L-V_NwCWO26qMQaOYw=w1426-h1901-no)

Then I needed to cut 6 player trays. These where missing a single divider. After
a couple of emails back and forth with Midgard Creative Studio, I managed to
convince them they where missing and they sent me updated files. 

![A completed player tray](https://lh3.googleusercontent.com/bAnndY_aBelT3beZLvnV3G5UiqguSLhh7mhjmT83IsjB8phdT4NyA5W7EvdiCH_HDsh0RzfHsnde_Te2oXOL8yAMoLX5Ef5dvueyRDHrICB1Lwa72NprgEmuFivBFiBIioxRzg1QB7iZJ7orfIEcRUBXZANViawQR5K-YgQWlqR0eThlqNqL4_X04C2zlBdusqIh6pruRDSKEOpeyYMHCyKbJhM7e9tDDsGjzqeRjVcFDs8aTXIA0AeItr5LCrmGDJTusGGEZCKklXik_p3v5l_2otJgAcUnLW0knKkTfX1hAWlHupyKuUrWQmZQ__Le7yap1eIHoGxOoR2QNuzpM1JlhDm5vPcmOUQVX4Im_8MyNclUeNXteLCqZp-uiZcGQZSbdOPODl_zWl5s5M-rxbu8wyuEoAircfS-KCdclJLVF6_5LL3s73HhQ0FxynPlE3iiDacVXCfCRqjYzLfuOXTOvXZx0bOsc7IwfLFKj9-Cf-Zwl2xMtHTB3_hTRZNNnf6ePredAPV-FT2_bFAJjEdh7sXqye_bSk-roWZ_EhS2glrYK1iU5trMsrpCbNDMMt_LvewiMwpxHBtqgUhRJb9Ke6taoTqFRMuGF6lukFRBM38sv77SjxOyRBH-lAbnDoLslMHwKwgOwJwSm3dC49tok-9Upt4SPn09hHwyFztXbfbJ1LxQJdxMyqaHFRXaZdrlbzIvrsXsNm6O7pmXXf_G0w=w1426-h1901-no)

![I believe I used this material very efficiently](https://lh3.googleusercontent.com/bTNzG4tZzNnOaTAItu-4Z0w9mHKZXelhFpW1xAuLYhkjb6lQi8yijMd_NuS8tnF4qpwNq2qtQf8IpWYID8vHUTWnHDr1_sNuO9whzAr7JQ8xJOW8mxDTXICrTL4QzVDBxZXyY0_AWLpji4OhRvF6qOqs6DePNd1BbiapS89IP9hyL2FxmdTkhK2k7ODeep8L2rmFtHMBmQ2p0ned8thYOCHPPxWssSEMTie3hSTQUTeoQdFaMb6dvIn1_Mzywq-NKbSuf8LmFgK7DjtbO239jgsyCFPwSWAcLUOsPG09zdK8G-kvQTP3gKXD6YuImKbdMi-xORqpXndr7DDDPKXG4NOPL9dxcp_ZGSKOeFxJoY-ysJgkr-eQ7IWOE5ByZJq3GKM7C3y1PHWzbfUpw2G_cxVO4pVR4-0IILxN6znRv6slV9K5gVBg8vekVl1268IYRrfhuEfPjPN7cNv-sujd45pzQlOuxTAf_rxabtCvcy5Ns3pEMrD44tzE1Od6SuYaN5qoSMvbfMXwmcX_13dCseGwdDDiY5fmgR7vw7N4WCq_jhhTgNFIjNwqU88l_US4zcwE0d8-a1gIR3Jaqki1zBrGe7vRLmt-EnDIzkBVlnpzAkVcVNRv1x-K6KxoaRtf_6_UyavHZsK6hfBv2gm7gV5mTVL5KFLTkFvIhTzzbeojnF8w5Nmpm0Bs-BLGsbrgAH2YKppwj_uLL0Ei1mWNt_zp7A=w1426-h1901-no)

Last few inserts only needed one of each. However even one of those where
missing a part. I never convinced Midgard Creative Studio that this part was
missing from the plans, so I ended up designing the part myself. At this point,
I had enough experience with Inkscape that I could make the missing part myself.
Rather than wait, I synthesized a new part using bits of other parts that where
included. It fit nicely into the slot that was expecting it, so I think I did a
pretty good job.

![Complete inventory of cuts](https://lh3.googleusercontent.com/SG6A_4zSB7Y5G50F4ziZplxviNgPPeJ7wxO_uTZMmBKhIruRww5iyVaRw42H9hWjag3CZbe4PF2NRxsrFnxXygmya5GnU-24Onmlk6ZZU-fgvz6v31X45gKuH4e_CcZYKspfJTfvWEBAjyz1HeLFY_OYkgQsB8goE2vdqNr3fJotaYnNxEWnz_sx-YCMffLqOl-Rv--tpPTfX7anjezt4k4T6PVU1VneuoYLdNXNXYwbycJQYWOINZJzO1N6FqAX3yglKHI_p9Ed86vmJc0aZ2O2LevukwEJ3-d5Mf08oGIQV81ndk8vdG_wESrzAGWCgq8WlFsj5QVmqbtWC2W3KxZ4WoqUoLydAjeExKtWQWivQKORwmo_tZ8-d0kuUt-GV4OT5aJwZx-yzrrDiZpubvQSjJxj3K4p0jwtWLymRSn0EXDCw6YWwyHaHGGK0ja3hDoHGC_73gcJrw2Rgo5_pK8Ulw8lp5hYZm6AlZ71TVRg2MJeTHEs_KJNwsjCryLrVJJPfaI6V0_1Id9Rn4YmnlEQUKEwTaEUzZ5a19cAr08eVzGCIIAus41y4fYS9l_T1pTlpP39wWAM0yYy0MucV9sy5JWl3Nq_Nd5qgu1Uq7fVmuktq3dMbOU-wIEvrjP-pZmaNW3yAGDf4fOk9OW41pk0TnDAuTzrys3UPqZ-UC91yu6e2tDDb_R4kepKn7zeg0-i0YKsMVccCcvmYjISG0b9ww=w2535-h1901-no)

When I got home, I noticed I missed a hole. This error wasn't anyone's fault but
my own. Arranging and copying cuts getting ready for the laser, I mark-selected
a section and missed a hole for this one piece. I discovered this mistake while
glueing up the project and had to return to DMS to cut a replacement.

![Black circle around to spot a hole **should** be](https://lh3.googleusercontent.com/twltGGhTVTEwsjFH4DBtrdQMwRoctatj6oNRRqj8PinmAZmmOPyjBfERVe9Tj-Y27zYSVvHrOO4Ghf-WNAaf-t01ZNlUb3_qFEpyRTFXXxGmXQFOjmXrfMvMTNSjCeS24xZPsPmJx8Vq70M-gtDIv3-HHO5x1zOQukoEobYY0etuCeG9ixUGDVLr8f_xBaZhWX_zRZSrjRTLr23a2rK8388Np98BKXZh1b-4i4B53o_mZJuGSXArsPUudb-hJ72Hz_9ISbaf-Wdly21sF_TaN_X9v_GTtd3bWk09lxy4xpoIyg3R8dxPYhMHqABz9YgZePXu17CRgda9O6ci1qzlUzFUy58sdkRs40lt5IifdhJeUFhyARb9yFOMMTkYSDThB_ohZj79qaY9VMZ7aIHcm-eD8h2zWbVnPEtJ4FzobuHoe75EElm4IAUx5-Hw0GzJ6VdYraX21RFtRKvNTDP2vt8K06SsjQU2RbRZHR77yMWpN-d8Lo3DCH2tyb02q6x21kaI0TCopT9u4BujwLtKoQ6vqT5h06h_cDNXjcQmh_fyYDhicFpXaBl9K4libpKY7HrOynBkBYxTiR1Kh6Lj_SzpP9XAW_Cg0c6xD5XTF8UrcEWfxqXHrlv2Hk6GHCXNHujDhOeY-yqCFNlG6RXrBRwU4i7r_2XeUH8dE9lpT5LGUDDZiRsbIMeUroL8BJ8Qk-3xp77vNlfJFA_BInN11SpZDQ=w1426-h1901-no)

## Glue Up

Glue. Lots of Glue. And rubber bands. I stole some rubber bands from my roommate
to bind together while these dried. A day later everything seemed pretty good.
This story is probably best told as pictures:

![Keeping the table clean](https://lh3.googleusercontent.com/_V0kecoDB37NkrQD24LxitYUPZadPd-f0FhxOFt26eu6oKAieMvYgvwO6bLz8cTFPhUmvWwOA0z4-JdpT0yYM8l54GJxIUCuHQrpuimUwNAYXleJW3YsDCyPd8RF9WnZTvkKtsUgQENcZfD2bR3FEuidYWs7mDoEJGeY_PaRtf3QjBVDCbXU4tdy7KS_SqDcaHCUOHXp39d3Oqz4hx4FNhzStumqjzjiI6K1Srj0zCPNT27SBCJz4Q3zSBob5seNIZie0H8CA6nIsLv6k_xjjPootKA98UtdoeyofF3Hg0TdcGdWSjhOc-gGoe2b9wtVNuM9Z-SGAjlKm0ytG3b_uV7n_BbpjvwccL2x1hsQoPSRW7YgZMEWfJH2N27Lk3eTxbCfUUOnnYJBfXAIjt2zVvEYZrgQfYGL6elmHVo2kJ4gUPOAztQLm20YL_dTrzyY_WuZWOBwWk8GNQfL_n9oiGvs5vaXwaS2djKv-vpIWATBXR0AO0QXv-bibkUNBhaToMSp7t78GGnW94GlNRLCYog4nmkqBQoeISvgYYuGur1hnKK0aK8VCMMy4KNlKF7LCDkYgEtOwwot81yUVIziWSW_FC18rL8lyOxTzOBH2RCXF2nEe2gjPsK3eJvkBok8iHshQGsXlqr_VSGauEgosJXucXsL4bWSGKiEVpmAT-tA5kvyO_9En_gqAo9TGW-oodApSa4FWMCtiYIlSRokfRC5kg=w1426-h1901-no)

![This part was a little challenging to glue up](https://lh3.googleusercontent.com/Sg3GJ8opFPi5DrQOd3aDqE-In5IWwtOM42e5TgoT023oW86g-vVP3NQyyJ4OtexNTLfCujCQfpwNFhjErWmVA7pOHwjVmenPuknCaPBjmzg1-4q5V02lY6JVuVodF8EoaFc3kEMic3Kkd-fQoDpXJbhVdQXmMGUhHFFH6IDAXU-9j-EwYtkUiSES9CofM742Y24GIzhejLvGpdffLXuGx2Ywv2gC4S_9GyC7TCl66fIC_p72EDUDts2MkGG_JB5wOKCjuiYUFucpTSf_GO4wKANHPRmoaugdufm7-yujCgejpoqda0A_AD2iRb5eWCbK3TbBNMcGoAUdW4iiqQ-ZSa8ZnnpUW6Iq4G5mrYzC_LoFIY8INOlZbr42sYRKrEblRVRj-t6K5SF6iUy9rhKuwsHlYYS3U2duckvAnd31TwfZ-mS9U5_a6RTRAr7J_mOB6w6ySrgdas5jpCnZCUb-b8nE9pmdvObNXiRBkl0TDQ-WIiFvSYllfv9dsFP6MnA15FKQ5nIDY5FDV6c_3tDkN0nKZcpxAkHrVdf_gsitg6g5iV-X3ZMMlG1Oqh_j9GFVDxL_YoL3KcvnnUrjIpxDexPJOg6kp1UGlU70bVY-UQZJXqN7MGuCePS-xT4sqVdOouqLGE2t9twMnUscu_-02PsG6z8lFZyRdlZeQ30Q16aAE1hnTs8LSx8WL4LYLYL_cbfE3urmHCnfBwvrjABadfa08A=w1426-h1901-no)

![Lots of trays glued up!](https://lh3.googleusercontent.com/6r1EpRHWzdJlAU7smGbMYNPSM53IowdlYHoHdjLqok3ppg9qmjAjQc6S5YfRRO0X243RZJ5maLcuAlq2PeHO2XrpvXOrx-UMrodjvGXHZ28Bd3soqS9cruExqo4lI1BGSJmNLxLhJwh96kY4z9tuYZpVvDQ39ObWa4wN4-dFvOGoxUNVE_eV-vTLJPwaqZFTqoty0Ezjmhn4_Tg8yo1N7jaulslrOIqKDMZHvZn5KyXZsPIfv7qA39HtTkR4qwXUOxtUD8_t718rVXjpTOMoEX6PrgTKwrU71LDTv9gMIFWL1ktaQ22vtn9so86JXZrDOjb34j4cIz0Cnv8LNprAw_VUvt0PgbR6s-rq7X8fIiz7xqidEoZzkOpV0AiK48vNy6i1SGQoMeaH3vMQJGxccGyi9Tg-nuk6HI47fTUWv9Gcdz_SKfCbLxLvKm5IEYs9KxysPmNKjILVRDMujRZwjhnKQfF8tEt5QOCb66QYr6tcbUXVS83WztaOSAhxa3mYDgleV7QK1-NzJ4EBPi8A6ckyJkJGtYE9bMJmY7I07GJsaWaDNi12iSFGAbjMWbfIhTQurzJti3qqEdEj29iD_VAXa_nUOSlot3EoimkGE7KNzOj9jst4bNDzKeayXdJ1qhLkKtJDAVJOqiWCidbvo62gymWIK2FkGCHoARql_kivDB2Unv_W8DQA_Te78h7gTUJd0IrOapzhL7evHeQAoJrp-w=w2535-h1901-no)

## Conclusion

I like how it turned out. Once it was all finished, it made playing and cleanup
way easier. Everything has a place and it all fits nicely into the extra thick
box that the game came it. While playing, the 10 ship types didn't require
nearly as much table space as they did before, making them much easier to find
without being a huge mess. Cards has good storage, although maybe there is a
better way to sort the cards so technologies are so messy while being easy to
track which have have and don't. Planets could use a better "used/unused"
organization.


## Midgard Creative Studio

I bought these plans from Midgard Creative Studio. I like the idea of buying
plans and sourcing the materials yourself. This project was fun to build.
However, in the end, I was not impressed with Midgard Creative Studio as a
company. Epically for the price I paid for these plans. First they where
designed for a thickness of wood that was a challenge to source. Then, they only
allow you to download the plans once, and the licence strictly says you are
expected to purchase a new copy every time you want to make another insert (to
be fair, why would you want to create more than one?). Last the plans where
incomplete, two dividers on two different trays where missing. On the plus side,
the plans where already in a format that the laser cutting software could read,
with cutting speeds pre-programmed, so I didn't have to guess which parts needed
to be cut through and which parts should be etched or how to do that with this
material (I believe he uses a laser similar to the ones at DMS). 

All in all, I spent about $18 on plans, $40 on wood, and $20 on laser time for
this project. Broken Token sells similarly sized completed insert for another
massive game, Gloomhaven, for $80. Midgard Creative Studio's pricing means I
saved nothing. However, I do have extra wood scrap that could be used for
another project and I learned a lot while working on this project. 

It seems like Midgard Creative Studio used to sell these inserts directly,
instead of only selling plans. However, at the time of writing, it seems he
isn't even doing that. It seems within the last month, they have completely
stopped selling board game inserts. Which is a shame, because while I was not
impressed with the product for the price, it was still a fun build and I learned
a lot from it and they where one of a very few companies doing this.

Edit: I sent an email inquiry to Midgard Creative Studio about what happened to
their inventory. It sounds like their in talks with another manufacturer and
pulled the plans from the store for that reason.

# High resolution Gallery

High resolution source of all images captured during this project [can be found
here.](https://photos.app.goo.gl/sBWoxBKCSvVHvc81A)