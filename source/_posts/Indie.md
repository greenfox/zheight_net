---
title: Independent Game Dev
tags: gamedev
---

A few weeks ago, I quit my day job. Today is my first Monday where I am working
full time as a game developer entirely on my own dime. Today, I'm just going to talk about projects

<!--more-->

# Projects

I have a few projects and project ideas. I need to take inventory of the skills
I have at my disposal. I know what I'm capable of, but I will need contractors
for things like art assets.

## Tankroids

Tankroids is a Game Jam game that takes inspiration from "Tank" on the Atari2600
"Combat" cartridge and the Atari arcade game "Asteroids".

The Game Jam theme was "Arcade". I designed the game to use an arcade stick with
a single "fire" button. You use the stick to turn, move forward, and also slow
to a stop. While under trust you turn slower. Each of 3 ships have different
handling and shoot a different projectile.

This game was developed in 1 week. I was given 2 weeks, however I had some
issues acquiring the art needed for the original design. and had to start over
with a different design.

You can play a [two player, one keyboard version
here.](https://greenf0x.itch.io/tankroids)

The [repo is here.](https://gitlab.com/greenfox/july-game-jam)

If I continue this game, it will need:

- Art Overhaul: This game is built with stock assets. I believe an art style
  modeled after a vector monitor would be interesting. 
- Scene Tree Overhaul: I've noticed some design problems with how I handled the
  scene tree that could be fixed to make the game more modular and easier to
  network.
- Networking: This game is a local multiplayer game. Perfect for an arcade
  cabinet. But it has no shareability.
- Touch controls: People use phones. I need touch controls. It should just work
  on a phone.   
- More ships!: I need a ton of ships for this to be fun.
- KOTH Game: For this game to be a drop-in drop-out game with a dedicated
  server, I need a game type that works for that. I think a KOTH style game does
  that.

## Competitive Miner Game

Two teams fight over a single vowelized asteroid. This game is a FPS in zero
gravity. Gravity is arbitrary, any surface can be walked on.

The map is a procedurally generated asteroid with two space ships positioned on
opposite sides. The Asteroid voxels would be done with some scalar field like
Marching Cubes instead of Minecraft-style. Perhaps a procedural BSP geometry? 

The secondary objective of the game is to mine resources and return them to your
team's ship. This allows your team to buy better weapons and tools for the
secondary objective: to kill your opponent's ship.

I have described this as an "FPS", however, I'm still unsure if it should be a
3D first person game or a 2D top down game. It was originally conceived of as a
twin stick shooter, but this morning I had the idea of making it an FPS in the
[Shattered Horizon](https://youtu.be/Z_d24xkVRIk)'s style 

## Ship Logistics Game

A top down game where you collect resources to build a space ship. The game
leans heavily on logistics. You don't just have to cockpit with a engine, you
also have to build a fuel tank and pipes to connect the fuel tank to the
truster. If pipe isn't big enough, you won't get enough fuel. Every system on
the ship is managed logistically. Various fuels, power, steam (for kettle
thrusters). The ship becomes a grid of pipes and tubes between every major
component. 

This concept is heavily inspired by Factorio and Space Engineers. I realized all
the amazing complex ships that make up the Space Engineer's fan base and how few
of them will be really fully utilized. I began to think of how one might
redesign the game that could actually use such a complex design. My solution was
make the game 2D and then add a layer of logistics to encourage complex ship
design.

I was hoping to also include orbital mechanics into this game. However, I'm not
yet sure how to make that work.

## Inko

A simple puzzle platformer. Some form of this game has been rattling around in
my head for years. At this point, I'm not sure I should ever build it. We'll
see.