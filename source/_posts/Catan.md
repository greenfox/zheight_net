---
title: Catan
date: 2019-02-17 14:29:33
tags: [laser,"board games","games"]
---

For my first major laser cutting project, I found [a Catan board that was
entirely vectorized](https://www.thingiverse.com/thing:607829).


## Vector vs Raster

Vectors are lines. When you draw a line with a pencil, that's a vector (not
exactly, but close enough). Most images on your computer are rasterized, meaning
they are represented by individual boxes called pixels.

For this project, I needed a Catan board that was entirely Vectorized: images
made up of lines, not pixels. Laser cutters are very efficient for Vectorized
graphics. Laser cutters will work on Rasterized images, but they take longer. It
basically looks like a traditional ink-on-paper printer, in that the laser runs
back and forth over the work piece etching away one line at a time. That takes a
lot of time. However, a Vectorized piece just follows the lines that need to be
drawn. Vectorized cuts are less detailed, but much faster.

(it's probably worth pointing out, the laser itself never moves. The laser is a
massive 130w water cooled glass tube that looks like a neon sign. The only thing
that moves is a set of mirrors. But for simplicity, I say the laser is moving.)

<!--more-->


## Material

For this project I used the cheapest and thinnest wood I could find at Home
Depot. At first I chose this because I literally couldn't find anything else.
However, now I have some experience finding wood and I will likely continue
using this wood. It is very cheap and it's available at every Home Depot I've
visited. Other woods require traveling to the other side of town, adding one
or two hours to my projects. Plus they often cost much more. For **most**
projects, this material is probably "good enough". It's nearly the thickest
material that the lasers will cut and for more delicate projects, I might find a
thinner material.

## Test Cuts

My first test cut turned out well enough. After a few attempts, I figured out
that for this material, I needed 100% laser strength at about 10mm/s for cuts
through (although, some machines at DMS need to go slower in the bottom right
corner furthest from the laser) and about 10% at 300mm/s for etches on the
surface.

![Test Cut](https://lh3.googleusercontent.com/VWheSIVWrjhQWtiEKGM-VwTQM8w6EnbYHynug6Fs7RTmAcM3MFU6ln7ZdTOkdRGwK0pVUzTbm3Al7mGayXZ0n460PX2gRl0er8VRK0cy75lmVJbuqRPvSSQtIIDCDkf9OR1AkJZBLbD846E15PrWlsyKoW-QJKBV0fS7Lvlwe4tPkkFacmgWfp78wxdlBynMpRl5MtMsZhvhodbVlCX9LTlss-taF5JXp2kiOPJ5k6qLaCzYpm5QvkOrLZk2FXliCBlgah56-fHVyQlHs7qWvrjPJemZmDv3Adt8DVT_yxxRmd1R1RvSl91lLbe3u-IR_9o8Ii_kQOCUrq7qtS_MF2yj1b4ul9zPTkZgv9gQDPEa6JGajXB8VFpMVVaTlgeCEgdzwZW9qR-I2My9FXMCqUbQB1quA0zOWEf_fS_idRygx18m0oEI2bf9gHLJyzgLxyPgjuy8PL1JTx1pY-ls4U-rsna_DOas735ZkxNKQK0Nh1CBU9HZOEQuCtwEQg1ANbg8Dx57OkDhXoMPRCqwR_dYQl0qkiY4I1M8gTzpBlZpHDm5guQ5zd-rDUA9VBQnB99mNT-Jf8-Je8uTMLvhPQu0JgQnP93HXLBj8wGI62txwwPr1n3ez21PXlS343EYwz01insa-nNrLXtGgddJSaQoB7Q5dz7-7vcg2n74wFsMSLijpf8guK0mrhhnsCamOyQLEJjcrZOsk1IMRXDAayeFkA=w1426-h1901-no)

## Cutting process

The source files for this project where in the wrong format. While most of the
world uses \*.svg for vectorized projects, the software for the laser cutters at
DMS will not consume \*.svg. I used Inkscape to load the \*.svg, save as a
\*.dxf file. This is an unreliable export method, often leading to missing lines
and stripping any coloring (used by the laser to distinguish different cutting
speeds and laser powers). Today, I author my work in Inkscape, dump it to a
thumb drive, then load it up in Adobe Illustrator on the DMS computers, then
save as an \*.ai file. The laser cutting software at DMS will load an \*.ai file
reliably without losing data or color.

![Cutting Progress](https://lh3.googleusercontent.com/OgxBpReQOtT6K22ZIhyrAWpXlDgIBtuEsKe_zudb2CZHhACaY1cUt7mQHFlTAgFt3hmP4Ukubh_Wu1tFT_e10AHA1XRl7EXsixkVxXKP38RGeyL0yfMwft5JVetBsCXf9yKQS-jvlpAf8tRRjyqRv_LFpG7QZhRovHR-IgNRxwaITYqOVNLOITBBLjO9eSNMmzSc_0b9t7Pagafz2FLMzY7VOqh7Fb_watoJXBmKOHiWsZxff3KNLqgZVHv8CBUZhXY6a3cbvt-L7rk-v3wo8VW3ukV-yxLuGljdVrEJSjSMO6LvpfHiomEmJh2-03aATJ6Ksflau8EFpCOldbiBkyELjH5tc4if27Zp0kHVE2POq_kIQOAL1ShWEWF4shNjFfh9f4WF3Ruhzn3KMCVlrsJDCNrWmN1_dnjkea28iB2Bgx3C6iy3CHOhVBs_-UH8lVHYwASVVRxd-uBvUDfCPINughTXmO6MxFg3dEJ-i77tEmtPzxJDIG4P0zf3Uu8WtX2eGCGWeFOMZUU-AAS406NprF-oHNXeE9C79mA_VcpcmCXhGw3THYepVDoyKgZBbXVi-ZolwalhGbyqk49s8lh-7pgFvI0_ZywOIftm9P4HddbbWT8jzLvpmXJ4Z1hn6Ztkg3MMfWoppfsHNa-vTCeI9VOWoVy8hALh0kzVGNJxUKor0_X0NqSl__8zuSPAEI70EYZ0-6bO9SLUA3c_4l2kSg=w2535-h1901-no)

Once I had the laser speed and power dialed in, the cutting process was fairly
strait forward. 

![Finished
Cutting](https://lh3.googleusercontent.com/WReDHxmYwcJzoauj4PqYzOn-Dvw2vOSnCOs7nzy77lkl86bbf3-HULB7W6telt7OfWNt2oSFmJdUbEgzzkd1DvziY5maMenL2s-MTVh7e9z1wYHErAhZqTzWaMKCLs2dDXQg4neSQLbdPE3kEE92ogzkn6e0oUYNeGD1CBHc7TLO-BBlSGVbJxiELRjVixZL0l5zgx6AwlonXOKHICcw0Ay5i9fXacNlbSHssC_fmFRmNCUDjRKEe6KPnsrkVnK90JeMZocrufzkeUVcivOMLlZcLHNgi1Ocn_fajPNVRLmGIphrbhg9oWTi-A1zRcNuM84YEDt1o4hBXueqsj5hfAqSO0GH36b1k-uErX6z9oV4Zl3aJGMsfL8DpfLyYjqfrHFMcw_lWBIUtxsK1x3kGS7aOKve-h20uF4LyzZNvTdylbcLEBqY0phafiJ2s2qNKe8ekExDSfaFYYCgd__12xiYdkfERXqPmB_BH4dg_Mq8CM0847UshcMJWNcKPZFWXcirMh9nssvUnmuYr-7LzDm2_jr9Q-8Sf5a_4t2nuuHkeaVTYi7C3aRgw0ZBBEVZEWXNlbmuQrBkEP7bHY_yekOSk3VeeQKq6ZY-zoiB1FBM3-rbUH4oG0Hxk1K_vwwxJJhusaSKMoXZCwSfJLArh_uvgW6jgpKxTB-RJ_qoN25Sbo53NFFTf6QapT2rzBFHFn8LgRvdbdTQWWyerChg-2bLTQ=w1426-h1901-no)

![Taking Inventory](https://lh3.googleusercontent.com/jmV6vtrcrZrJQI93MfdlGgSa5LKZcM7Fq0vx53uXWDbVGZWSzn3Q-WnwiAW21EY_mBCGVGZl2_p51sfK6ywVxjmlSSN2K2sFKl2Lp1OBdQHGfMX1QbmwFOUjCOXk5sUywa1frmqem_hn3VaW5eSVJD2KqqwWsJFYl5Xbkxz9Ce8I4SPHulVv97kOvnRw4xS-5I0oulf7Yjeq_En5jdSUH9mkdL5g1WcIxKw7AXYI7wywr2NcPHXsNzg5OUtFMhDf1WRMkR4EsTUJU8g16dYfP0W2QGbY2sXO8h9W6N4nHNJ8MHo_5NJYGBYAsWMXb8MSHAkz9rlUqsDxAEwYgPvsIQ5VCSAu0mNa8XQ2bPSIWVIy68o3DlyHR8776zaX2kPHYEs07rfYLn2cwrKg81gzqvXxmLVRNRu21V_6r9D7m59Tke8DNUrw9xTaaqKIltgp4DzneQVLYqVmRzksTdvCEvumQ1xcBVq1VsENPcP7wntiGlmAdMel7GLeXom4fHnXcrf_ilsumq9UDJ-jrK-vtdRZc_XubAmQGNRsEHYvv5ailxkZ9KTrs1OFRvLY6vw6Jl7R4_2Wh4_GhXg8Qu6WF5r-WwRrl_TPan_n_VnLW1PWrOWNiBBxiPsX82nuoalaDytYim2kTNu7CubTHPxTWKYh6Usy6uUwccDsCmqqZA24D434oElQxq1D34AxGwBnargXTORQ1qpwhGQV1qFa7NuYMA=w2535-h1901-no)

They fit **very** nicely! 

![Testing Fit](https://lh3.googleusercontent.com/dEIQ2wLAQYysjJ08RrZdptEYfqAm2J4SZWK4T6JMcpScm3iu4RNiAfrGp83bprop-1XzjwSieBqkb5uBgHCW5U43b6nLMvH0Tdz6xEfpXqLSjQKiVTfwdIEthevBkj7v6701I5p8gD3TFxX3x-rT1OC8V7w0PneoBMMyzxOxaxkStEYP-xCkBN4B53HiXt7kiz4BczIdnfcy_ZPVi8HhGKSB8JOUkV1L-Aaq7ro6XLpuRYrMWM-Nu2PNpgE2W3wgZXK_scG_wLqDkQeVcX91ddmV3y1c0T0xqY2WaIvd_1_826EYmkurigjWFq5pXu6V37KQZ0WqCRVh3wNzPIeDsk6lSHc1HFMiXbrpoTJxHSmaTCJIqPJGdLt3ZAZOSlRRZS45XB57f54gUSwrfsZPi2S0Xl_oAfgSti89CzGhVkAr2sD-wtaz8k0i8_7vzaq6OVrnnrx0AL0Ufa8XMnnbqhSJDFALxamIbb_u0zYfT6qtZO2vE729-Z5Ofr-Ck-sd-5OuyJLq4r-bB8AjVsbMC5UQxDHT32JZyZapJ_kEv4xTWTHf_gVtDgwCPhqGkrDpbpg-7kHIPFfY1yrX14_fA5W6SzKUxl0ADDMJHKe9qT9pYud-rtTpQIAlX-qQfMhXqnXhRFQOCVtqN5eY-raWPCclfgXl-XaDdwJd9dwrOg1uzOCHfgm7mTtcqQavkGpWKV4N9bID7YWMGaftikP0udy_wQ=w2535-h1901-no)

I was happy with how this looked this far. If I did this project again, I might
consider this completed at this stage. However, I wanted to experiment with
finishes, so I continued. I wanted to create a game that had different colors
for each tile type so that it's super obvious where each resource is.

## Finishing

My first few test finishes:

![Testing Finishes](https://lh3.googleusercontent.com/eMJJsyB7ysZKmhcD1HqkIdajyxatxqR64IWsYdG2aHlnTGbVheYmTL5qEloCQIrBWANoawaUUC4LkSou52v35owXcWrZr9gMtaUMpe7CKoK3RcQx8ay6cMUK0jiWE7usF14i7tcYyW7b5zodoXaaq2SD5VDxQnqBjLFRcDNIQc7NbR-Bx9aXWT_CUcVPXIJucIytElG3zEtnIHbAUtrhtrNhwhpsssa_F7RqbLKVq9lV0lLG4gnKdJfMFbC_d6zlnQ12C3wZlyxWCdCRN_mLWntehJBhlnr8rlUILWvTiYvxIvzONCiNg4f5VbZoRZxcPMOmL5y6gtwjGzMe7B_kQ7ig8pXHpVUPYT-9Labu8JBX-4KaPH07b2ZPNDXzOUmiBGQXJsNBeMmllaoA-rDhnGPUQMpvzb0sgrK-S_5PkNLQhEKf-vav5h29JutiNIkD6sPci9ZuDCruagbGj-vxxC1xsv_TgDZq4tuj4BAMqMmnRQ_enalfvJXbxqSX_uk12aGl4z65uhYLFra0nkFXOmbSAPHFd_Ip3zwBHKgW35LXpthp0Jj3vr_pZYJHw_y3YbP28SpjHwDD6vtaF-qsKRqunqY8tYxID89hD8Yaf_Cml34Nh9Xf57SF2TyZnnnyNUDQfSjpA3c9Tqck3PAZXlDl6pF_ttiLHDYPOXNMGwwyqzkb94sKaT-ivzb2NlEX1KW6j6KEi_wwogS8cyv3Ho1qFQ=w2535-h1901-no)

From left to right:

- Cheap Spray on lacquer
- Diluted acrylic paint
- More diluted acrylic
- Unfinished 

I liked the 3rd one best. Then I got to painting all the hexes. For some, the color showed better than others.

## Box

After cutting these pieces, I decided I needed a box to store all of these. I
found a tool suggested by another member of DMS [for making laser cut
boxes](https://www.festi.info/boxes.py/).

For the first box I made, I wanted to try MDF. MDF is basically sawdust in glue.
It's not very pretty, but it is very strong. I thought that would be a good fit
for the box. After trying to cut once, I decided I didn't like it. It was harder
to cut through than the wood of the same thickness. Since I had to cut so
slowly, it had harshly charred edges. I hated the smell and it has a cardboard
look to it. However, I captured a quick timelapse of cutting this and best not
to let such a cool video go to waste! (It looks really fast, but actually took
about 15 minutes to cut)

{% raw %}
<video src="https://i.imgur.com/kOgn9JZ.mp4" controls loop></video>
{% endraw %}

Eventually I designed a box that I liked. Here it is after being glued up.

![Final Box](https://lh3.googleusercontent.com/Vy1NsVXfKQF67OjLwGU_S5e3HpkxkqKzOB2Jr0GWIg1tXnGmW5sEIymVhZPEYUTb4l16O7_yNo3vhRJS-jacjJ6K6BhPkbY2QHiFFsjiEUob9Abe0YAyLSW3tO9hm8lAab1Z0CcfoUN03OsfHCbPoZo3vJnZswl6yOtE-WMQvfgBClrQ12Jj1HjwHhxboy7bfjqGp2Xk1u_cfAFLuh4zVAS54sGgpzop1vrCeB4Z-C5jGVWO3OeW81XDw7u-6DFL6RMc8Vv0JF9jwDfx9W8c6BksS5Go6AYnl846Q-NZxxYfwz_fKDHEeS4Ug6SA9lrnpklirD0adfHVtclCo8bwh0ds2ZhGffrwi2Q1NDhGXim0wNPgpn8DzSCpLZM4SndmVZv0xo1P_zHHUvYWBl8rf9u2yezjUJRQOam50asJTddQgNRR0Ed4CLTyLv99p2erUrqKpLeZWK9azlfAY-iNFV31KKPCBiJILPUOt2H72TY3_U1GuRqENSxRNGz5rhDwgmqkOKDWwSe84qVEoTCC7tZzP6tfrbiB2zBMcDPnuX5yth5oC4qbUhMqsER1XaMSD3g-vxlpFQmtX7cXrMvZAfamh246oLqkifIin6_lQQ0irb8c140LWLbG_739lRRXehln8NCHgUCY9c85dDm3URwRyE9XjclMcdhVIyVdIZ-bLXj8ewmlrAcKxRhsFX6XcaDR7NHxVnGF6MslqQbad9so3g=w1426-h1900-no)

## Longest Road and Largest Army

A store bought box of Catan comes with 19 tiles, 4 sets of player pieces (roads,
settlements, cities, and a Building Costs card), a deck of Development and
Resources cards, a Largest Army card, and a Longest Road card. 

You can buy a box of the Resource and Development cards alone. Players don't
necessarily need Building Costs cards. However, Largest Army and Longest Road
cards are much more necessary. The laser cut plans I had didn't include them and
they cannot be bought alone. So I designed some.

{% asset_img catanCards.svg "My Cards" %}

I believe the design turned out nicely, If I did them again, I would like to update them
to have 3 swords and 5 roads, as that more closely represents the game.

![Final Cards](https://lh3.googleusercontent.com/wmfbSpckZVnhohLxJN5vxahiPl5BP0VO6DK_RuejMsQVQqsARawzSrj3WK9OPTtU1cpO9z9_oThFFInEbQJzMxWPLCEjnvgKUVim4y-zv1WSYO7Bu-VquwmQhacz4ixkTunZyGQ85cxQVtZctUTuuFvyG59d-B2Kfm0I5eRCMaJEZHIW7V63vztFAREXt3-k6HvdGCXHlHAmv70lid7vrydE-Ua3AYXyem4e7rXG2KgKo21WJs1LmWDX-XMqZkZm0fF_Z3JmEDIwP6NBi6LgGD4ZDXcfJ9qs1PJ9pqhohOJEsIGXMq4P7QcC9MaMqZFqRobdGO050hH09fclr1tFxVoQG55nbF_hpZ453OUW5caPVNJA4NRs7AZYqXg_Z4RJvQvVzJT2hUz-YYQHzwFdt6OtV1uogR_6zLT1WRkyKBzZ3RnraqqJgbVEtqoG7RPpCG7XGaeTT1m4SAPJTU4G6z8s1HDWONP_y2pn-NcqGNANVRRx4_NAZEmuewhuxb6KwdRo8ZTzwHMxBayMLUIwNo7ZkQA51sG1yXxsbE1V6hqRu6DR_69UmBROD1yr1B1EujBO39BtYM2dAVQFdzt2aBmiKj0JcsdFip9999-3_B8vc1fcfDFNvtih5Eiw8GX-mOTcFeLgsxdoxTGhEFE4pI-SS2LmZ977NWvGcZcmq_6wxZqlj2U-3eCzKc1qQRlDcuN2JtQwQgtSeikhpqY-VXrk_g=w2535-h1901-no)

## Finished Project

I haven't had a chance to play it, but here is what it looks like fully
assembled. I really like how it turned out. I'm already thinking about what it
will take to make some of Catan's expansions. Seafarers seems like the obvious
next step. 

![Final Product](https://lh3.googleusercontent.com/5zn9ZdSmnhAP6cHTt0vQASNBD8q3Lq9-FIa3a0KxazCi-4655WI6Gye2IMThPzLJCh_g3reZJ8GIXN2CxK5QlurhnmjKvlKfaMrj5npfApr5D9M5yM7A4XPUbtKUKSgEd1EAoO-kiXQcIYAmJrF1v_AHLqcw5pwLl3QZt5vvygY_tW6UBporhgTs-anWeesbv406S741LIe_mwkEqKABCbjylY5imXxiluU5WmE-6ny3D5j4d2pS8rOcJ2T-7cPX4yBitTROyLHU8geDesapZoKsChTFTdHihGhuKO4G05C-3-u1B8yoUqWvJSegBOwwt8jDCvOnV1qC2Sw8c6yxKoU-lRBbZTWNKqdGSECKDt_7MEvsYLnNyO9nUoa-Hd2g-tGTEGCE537zHvieAUZu5J3BhO4N88nAnO_WaqpkI4GnMbGqO-NvQNs41g5_Bne_BNfOd2GPJqAvZuN6eDZHMRmYuEta6b_XVoLL7IjyPRy1l4UBIeKXbble15z-z_-bKCd-cCo8DLKnKPT6MkTNyDk8PSPpSrh_EN_nrQyZPOWC9oadt9PlM7YKEBnVSPQHz354fSfM1-nYBpGaD2UQHytMVSTEe1BC24Tmv9-V2sUL26JCMxmQ75JFBkCsLKAAl7_DPLJ_IMYXGOz5UxN_xmkWVRoBIucK=w2535-h1901-no)

## Next Time

I'd like to find or commission a board that is more similar in style to the base
game. Most laser cut Catan tiles I've seen have icons that match the resource
the tile produces. I would like one that matches where the resource comes from.
For example, rolling planes for sheep, forests for woods, etc. The tiles that
come with the base game are like that. That could be too challenging though. For
full color cardboard pieces, it's easier to make it obvious what it is. For a
laser cut piece, that might be too hard. Perhaps the paint is enough to make
that work.

If I where to do this project again, I think I'd consider using [these
plans](https://www.thingiverse.com/thing:124291) instead. 

I still haven't created Building Costs cards. I believe I'd likely have to
create those myself. At the time of creating most of this project, that was
beyond my skills. Today, I'm much better at Inkscape and believe I could make
some decent cards. 

I'd really like to find a thinner wood next time. These tiles are thick and
kinda hard to separate.

Overall, I like how it turned out and I'm excited to see how well it plays.

# High resolution Gallery

High resolution source of all images captured during this project [can be found
here.](https://photos.app.goo.gl/cug1WZmDsuRsxr176)