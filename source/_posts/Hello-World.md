---
title: Hello World
date: 2019-02-03 14:54:26
tags: [ "Hello World"]
---

<!-- # Hello World -->

## Welcome to ZHeight.net

I have owned this domain for several years now, but I could never think of what
to do with it. When I bought it, I thought I was going to build website for
watching 3d Printer video streams. After some testing, it seemed like no one was
interested. I parked on the domain for a while until it expired.

Then I saw [a video from Maker's Muse](https://youtu.be/-nesZt9igME) and decided
I should blog my projects. I bought this domain back and got to work.

{% raw %}
<iframe width="560" height="315" src="https://www.youtube.com/embed/-nesZt9igME" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
{% endraw %}

<!--more-->

I'm (for lack of a better term) a tinkerer. I poke around in a lot of different
spaces. Laser Cutting, 3d printing, woodworking, and programming (my day job).
I'd like to put more effort into these projects (and less effort wasted in
gaming).

After some hunting around I [found Hexo](https://hexo.io/), a self-hosted
blogging tool for programmers. With this, I can write documents with VS Code in
[Markdown format](https://docs.gitlab.com/ee/user/markdown.html), see how
they'll look on a server, `git commit` them to GitLab, let GitLab CI/CD tools
run and compile my blog, then auto host it on GitLab Pages (this has limits, so
I might although I might do something on another host with docker later). This
is the perfect tool for me.

GitLab's build automation is so simple, that my entire automation is just these
14 lines. (if you're not a programmer, this is meaningless; the point is that I
had to write very little to get it to work)

```yaml
image: node:8.15

pages:
  cache:
    paths:
    - node_modules/
  script:
  - npm install
  - ./node_modules/.bin/hexo generate
  artifacts:
    paths:
    - public
  only:
  - master
```

(oh, and look how awesomely Hexo handles code within a blog! Syntax highlighting
and everything I love this tool!)

I'm using Visual Studio Code with a spellchecker plugin and a code formatter
called "rewrap" (ctrl+q reformats text to be more readable) on Ubuntu.

## What I expect to Blog

I hope to blog nearly every project I work on. However, I'm not sure what that
really means. I think it boils down to writing something every day that I do
something related to project. Maybe if a project takes more than one week, I'll
create a "progress" post. I don't want to post so often that each post isn't
interesting, but I also don't want to post so infrequent that it no longer
becomes a habit.

I don't expect to blog anything that goes beyond hobby projects. That means I
won't blog my day job. If I take contracts, there is a good chance I won't blog
those (unless it's interesting and the client is cool with it). I may blog
something that I might sell (there are a couple of things on that list so far).

## Types of Projects:

### 3d Printing

I have been 3d Printing for 5 years. I bought a crappy Prusa i3 off a cheap
reseller back when $500 was crazy cheap for a 3d printer (today we'd call it a
"Prusa i3 clone" but back then Prusas where clones). It was poor quality, and I
ended up replacing nearly every part of it except the motors and steel rods.
That process taught me more about electronics and robotics than any class I took
in college ever did. Currently, however, both of my 3d printers are broken and
will require some work to fix. If fixing them is interesting, I'll try to blog
it.

### Programming

I have written a few personal projects that have ranged barely a project to
currently running in production. However, I'm not sure programming would be
interesting for most people to read about. I might not blog them, or I might
just post final results.

### Laser Cutting

I recently joined a [Dallas Makerspace](https://dallasmakerspace.org/). For a
very affordable membership I get access to all kinds of tools, including several
laser cutters. As much as I enjoy 3d Printing, laser cutting has become a new
love. Additive manufacturing (3d printing) is great at creating complex shapes
that were previously impossible with traditional manufacturing. However, it's
very slow. Laser cutting is crazy fast. Within a few minutes of machine time, I
can create a very large, but geometrically simple project. Each has it's own
uses.

I think my future is a combination of 3d printing and laser cutting. If I need
something small, but complex, I'll 3d print it. If I need something large and
simple, I will laser cut it. If I need something large and complex, I'll try to
find a way to mix the two.

### Catan & Twilight Imperium

The first real project I will blog about is a laser cut insert for a couple of
board games. See you then.

#### PS:

I don't know whether I am going to call myself by my real name or screen name (or
something else) for this blog, so until then:

-ZHeight